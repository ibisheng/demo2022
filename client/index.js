import Vue from 'vue';
import iView from 'iview';
import 'normalize.css';
import App from './App.vue';
import router from './router/index';

Vue.use(iView);

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')
