import VueRouter from 'vue-router';
import Vue from 'vue';
import files from '../components/Files.vue'
import chooseComp from '../components/ChooseComp.vue'
import clientApi from '../components/ClientApi.vue'

Vue.use(VueRouter);

const routes = [
    {
        path: '/chooseComp',
        component: chooseComp
    },
    {
        path: '/clientApi',
        component: clientApi
    },
    {
        path: '/files',
        component: files
    }
]

const router = new VueRouter({
    routes
})

export default router