function getNowTime() {
    var date = new Date();
    return date.toISOString()
}
function addZero(s) {
    return s < 10 ? ('0' + s) : s;
}
module.exports = {
    getNowTime,
}