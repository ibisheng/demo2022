let fileMgr = require('../fileMgr')
let config = require('../config')
let signArg = require("../signArguments")
let util = require('../util')
let axios = require('axios')
let fs = require('fs')
let querystring = require('querystring')
let TOKEN ={}

module.exports = function (app) {
    app.get('/api/ping', function (req, res) {
        res.send("毕升文档 api demo server. status: ok");
    });

    //获取文件列表数据
    app.get('/api/queryFileList', function (req, res) {
        let files = fileMgr.queryFileList();
        let fileList = [];
        for (k in files) {
            file = files[k];
            fileList.push({
                docId: file.docId,
                title: file.title,
                fetchUrl: config.getConfig().fetchUrl + '/' + file.docId
            });
        }
        res.send(fileList);
    });

    //设置bso_v4_sdkToken
    app.get('/api/setApiSignToken', function (req, res) {
        let userId = req.query.userid;
        setApiSignToken(userId, res);
    });
    //对用户进行签名认证：调用毕升相关API时，需要作为token参数传入：https://wiki2.bishengoffice.com/zh/%E6%8E%88%E6%9D%83%E7%AD%BE%E5%90%8D
    function setApiSignToken(userId, resContext) {
        let ak = config.getConfig().access_key_id;
        let sk = config.getConfig().secret_access_key;
        let time = util.getNowTime();
        let sign = signArg.HmacMd5Sign(sk, time)
        //毕升系统中如果没有该用户，将会调用此地址以获取用户的信息，
        //如果不指定则，毕升系统将使用用户ID在通讯录根下创建该用户。
        let syncUserUrl = config.getConfig().syncUserUrl;
        let url = `${config.getConfig().bsdHost}/apps/api/auth/apiSign`;
        axios.post(url, {
            cert: {
                ak,
                time,
                sign
            },
            userId,
            syncUserUrl
        })
            .then((res) => {
                //业务系统服务端需要保存用户的token
                TOKEN[userId] = res.data.token;
                resContext.cookie('userId', userId);
                //将授权作为cookie保存,前端调用sdk时将会使用
                resContext.cookie('bso_v4_sdkToken', res.data.token);
                resContext.send({ 'status': true });
            })
            .catch((error) => {
                console.error(error);
            })
    }

    //获取调用方用户信息，此处为业务方提供的关于用户信息的获取接口,
    // department信息为该用户所在的部门。 该数据指定用户具体所属的部门信息，
    // 如果为多层，则office系统收到该数据之后，将创建指定的层级的部门信息之后将用户加入到部门中。
    //例如：下面的例子中指定了被请求的用户所属的部门为 研发部->office 研发小组.
    //https://wiki2.bishengoffice.com/zh/%E8%B4%A6%E5%8F%B7%E5%90%8C%E6%AD%A5
    app.get('/api/getUserInfo', function (req, res) {
        let userid = req.query.userid;
        let userInfo = {
            'status': true,
            'user': {
                'department':{
                    'departmentId':'myDevDepartment',
                    'title':'研发部',
                    'subDepartment':{
                        'departmentId':'myOwnDepartment',
                        'title':'office 研发小组',
                    }
                },
                'userId': userid,
                'uname': userid + '_Name',
            }
        }
        res.send(userInfo)
    });

    //获取文件的data callURL
    app.get('/api/getData/:docId/:userId', function (req, res) {
        let docId = req.params['docId'];
        let userId = req.params['userId'];
        let token = TOKEN[userId]   // 该用户的签名认证信息
        let dataStr = getSendData(docId, token);
        res.send(dataStr);
    });
    //管理页面app中设置的回调地址,office 文件编辑完成完成有新的文件生成时，可以回调这个地址
    //https://wiki2.bishengoffice.com/zh/%E9%9B%86%E6%88%90%E9%85%8D%E7%BD%AE
    app.post('/api/appCallback', function (req, res) {
        let result = {
            'status': true,
            'message': 'appCallback_OK'
        }
        res.send(result);
    });
    //返回预览文件的地址
    app.get('/api/file/view/:docId', function (req, res) {
        onGetBsoUrl(req, res, 'openPreview');
    });

    //返回编辑文件的地址
    app.get('/api/file/edit/:docId', function (req, res) {
        onGetBsoUrl(req, res, 'openEditor');
    });

    function onGetBsoUrl(req, resContext, suffix) {
        let docId = req.params['docId'];
        let userId = req.cookies['userId'];
        let callURL = config.getConfig().callURL
        if (callURL && callURL !== ''){
            //业务系统根据自己的需要生成对应callURL的地址,以供毕升服务来获取文档信息。
            callURL = Buffer.from(callURL + '/' + docId+'/'+userId).toString('base64');
            let url = `${config.getConfig().bsdHost}/apps/drive/${suffix}?callURL=${callURL}`;
            resContext.redirect(url)
        }

    }

    function getSendData(docId, token) {
        let files = fileMgr.queryFileList();
        let title = files[docId].title;
        let fetchUrl = config.getConfig().fetchUrl + '/' + docId;
        let dataJson = {
            doc: {
                docId,
                title,
                fetchUrl,
            },
            token,
            opts: {
                privilege: [
                    'FILE_READ',
                    'FILE_WRITE',
                    'FILE_DOWNLOAD',
                    'FILE_PRINT'
                ]
            },
        };
        let appId = config.getConfig().appId;
        // 如果指定了App，就使用指定的App，app实际上指定来文档在毕升系统的记录的位置。
        if (appId && appId !== ''){
            //添加App信息
            let appKey = config.getConfig().appKey;
            let time = util.getNowTime()
            let store = {
                appId,
                time,
                sign: signArg.HmacMd5Sign(appKey, time),
                folderPath: config.getConfig().folderPath
            }
            dataJson['store'] = store;
        }
        dataStr = JSON.stringify(dataJson);
        return dataStr;
    }

    //下载server中的文件
    app.get('/api/download/:docId', function (req, res) {
        let docId = req.params['docId'];
        let files = fileMgr.queryFileList();
        let filePath = files[docId].storage;
        let fileName = files[docId].title;
        var file = fs.readFileSync(filePath, 'binary');
        res.setHeader('Content-Length', file.length);
        res.setHeader('Content-disposition', 'attachment; filename=' + querystring.escape(fileName));
        res.write(file, 'binary');
        res.end();
    })
}
