const config = require("../config");
module.exports = function(app) {
    app.get('/', function (req, res) {
        res.render('index',{"bsdHost":config.getConfig().bsdHost});
    });
}
